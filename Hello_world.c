#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "K_MT.h""

#define SORT_MAX 100000

void sort_rand(long sort_data[]);

int main(void)
{
	FILE *fp;
	int i;
	long sort_data[SORT_MAX] = { 0 };
	printf("Hello world！\n");
	sort_rand(sort_data);
	printf("乱数を生成し終わりました\n");

	fp = fopen("data\\rand.csv", "w");
	for (i = 0; i < SORT_MAX; i++)
	{
		fprintf(fp, "%ld\n", sort_data[i]);
	}
	fclose(fp);

	return 0;
}

/*ソート用乱数アルゴリズム*/
void sort_rand(long sort_data[])
{
	int i, cp_sort_data[SORT_MAX] = { 0 };
	time_t t;
	long x;

	i = 0;

	while (i < SORT_MAX)
	{
		init_genrand((unsigned)clock());
		x = genrand_int32() % SORT_MAX;
		if (cp_sort_data[x] == 0)
		{
			sort_data[i] = x;
			printf("%d\n", i);
			cp_sort_data[x]++;
			i++;
		}
	}

	return;
}